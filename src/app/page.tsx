"use client";
import coreItemHardSkill from "../../public/dome/Item.svg";
import Dome from './components/Dome';

export default function Home() {
  
  return (
      <div className='App h-screen flex justify-center items-center'>
        <div className="p-[200px] rounded-2xl bg-[#1A1D23] bg-opacity-50">
          <Dome name="Hard skills" icon={coreItemHardSkill.src} path="createHardskill"/>
        </div>
      </div>
  )
}