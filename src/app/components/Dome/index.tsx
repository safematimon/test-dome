"use client";
import base from "../../../../public/dome/base.svg";
import floor from "../../../../public/dome/floor.svg";
import shadow from "../../../../public/dome/shadow.svg";
import track from '../../../../public/images/track.png';
import Spring from "../Spring";
import { useEffect, useState } from "react";
import Image from "next/image";
import Link from "next/link";
import "./dome.css"

interface DomeProps {
  name: string;
  path: string;
  icon: string;
}

const Dome: React.FC<DomeProps> = ({ name, path, icon }) => {

  const [cursorX, setCursorX] = useState(0);
  const [cursorY, setCursorY] = useState(0);

  const [hover, Sethover] = useState<boolean>(false);


  const mouseIn = () => Sethover(true)

  const mouseOut = () => Sethover(false)

  useEffect(() => {
    window.addEventListener("mousemove", (e: MouseEvent) => {
      setCursorX(e.pageX);
      setCursorY(e.pageY);
    });
  }, [])

  return (
    <Link href={`/${path}`}>
      <div
        className="dome flex flex-col items-center justify-start h-[320px] w-[320px] bg-aster-grey-800 bg-opacity-50 hover:bg-aster-grey-900 hover:bg-opacity-50 rounded-2xl ease-in-out duration-300 z-10 pt-[48px]"
        onMouseEnter={mouseIn}
        onMouseLeave={mouseOut}
      >
        {/* mouse follow */}
        {hover && 
          <Image src={track} alt="" className='cursor fixed w-[400px] h-[400px] object-fill transform -translate-x-1/2 -translate-y-1/2 ease-in' 
          style={{left: cursorX+'px',top: cursorY+'px'}} />
        }
        <div className="w-[120px] h-[160px] relative">
          <img className="absolute bottom-0 -mb-[36px] w-[168px] h-[112px] max-w-[168px] -left-[24px]" src={base.src} alt="union"/>
          <img className="absolute bottom-[12px] left-[8px]" src={floor.src} alt="vector"/>
          {/* item inside dome */}
          <Image className={`absolute left-[20px] duration-200 ${hover ? "bottom-[56px]" : "bottom-[40px]"}`} src={icon} alt="Item" width={80}  height={94}/>
          <svg className={`absolute bottom-[16px] left-[12px] ${hover ? "opacity-30" : "opacity-50" } duration-200`}
            width="96"
            height="144"
            viewBox="0 0 96 144"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <g filter="url(#filter0_i_29_1273)">
              <path
                fillRule="evenodd"
                clipRule="evenodd"
                d="M48 0.000244141C21.4903 0.000244141 0 21.4906 0 48.0002V124V124.326H0.00625423C0.424351 135.221 21.7518 144 48 144C74.2482 144 95.5756 135.221 95.9937 124.326H96V124V48.0002C96 21.4906 74.5097 0.000244141 48 0.000244141Z"
                fill="url(#paint0_linear_29_1273)"
              />
              <path
                fillRule="evenodd"
                clipRule="evenodd"
                d="M48 0.000244141C21.4903 0.000244141 0 21.4906 0 48.0002V124V124.326H0.00625423C0.424351 135.221 21.7518 144 48 144C74.2482 144 95.5756 135.221 95.9937 124.326H96V124V48.0002C96 21.4906 74.5097 0.000244141 48 0.000244141Z"
                fill="url(#paint1_linear_29_1273)"
              />
            </g>
            <defs>
              <filter id="filter0_i_29_1273" x="0" y="0.000244141" width="96" height="151.747" filterUnits="userSpaceOnUse" colorInterpolationFilters="sRGB">
                <feFlood floodOpacity="0" result="BackgroundImageFix" />
                <feBlend mode="normal" in="SourceGraphic" in2="BackgroundImageFix" result="shape"/>
                <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
                <feOffset dy="10.8465" />
                <feGaussianBlur stdDeviation="3.87375" />
                <feComposite in2="hardAlpha" operator="arithmetic" k2="-1" k3="1"/>
                <feColorMatrix type="matrix" values="0 0 0 0 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0.1 0"/>
                <feBlend mode="normal" in2="shape" result="effect1_innerShadow_29_1273"/>
              </filter>
              <linearGradient id="paint0_linear_29_1273" x1="77.1324" y1="7.32133" x2="6.13869" y2="48.8173" gradientUnits="userSpaceOnUse">
                <stop stopColor="#EC764C" />
                <stop offset="0.510417" stopColor="#7D25ED" />
                <stop offset="1" stopColor="#0183E8" />
              </linearGradient>
              <linearGradient id="paint1_linear_29_1273" x1="48" y1="0.000244141" x2="48" y2="120" gradientUnits="userSpaceOnUse">
                <stop stopColor="#2B303B" stopOpacity="0" />
                <stop offset="1" stopColor="#2B303B" />
              </linearGradient>
            </defs>
          </svg>
          <img className="absolute bottom-[20px] left-[20px]" src={shadow.src} alt="shadow" />
          <div className="absolute bottom-[16px] left-[11px]">
            <Spring hover={hover} />
          </div>
        </div>
          <p className={`text-center font-Poppins text-2xl font-semibold leading-8 z-50 mt-[32px] transition-color duration-300 bg-gradient-to-l from-orange-500 via-purple-700 to-blue-500 bg-clip-text ${hover ? "text-transparent" : "text-white"}`}>
            {name}
          </p>
      </div>
    </Link>
  );
};
export default Dome;
