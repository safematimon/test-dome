import vector1 from "../../../../public/dome/spring1.svg"
import vector2 from "../../../../public/dome/spring2.svg"
import vector3 from "../../../../public/dome/spring3.svg"
import vector4 from "../../../../public/dome/spring4.svg"
import vector5 from "../../../../public/dome/spring5.svg"

const styleVecter = "w-[98px] self-stretch shrink-0"

const Spring = ({hover}: {hover : boolean}) => {
    return (
        <div className="w-[98px] h-[144px] flex flex-col justify-end items-start shrink-0 pt-[32px] group">
            <img className={`${styleVecter} ${hover?"-mb-[33.14px]":"-mb-[25.14px]"} duration-200`} src={vector5.src} alt="vector1" />
            <img className={`${styleVecter} ${hover?"-mb-[33.14px]":"-mb-[25.14px]"} duration-200`} src={vector4.src} alt="vector1" />
            <img className={`${styleVecter} ${hover?"-mb-[33.14px]":"-mb-[25.14px]"} duration-200`} src={vector3.src} alt="vector1" />
            <img className={`${styleVecter} ${hover?"-mb-[33.14px]":"-mb-[25.14px]"} duration-200`} src={vector2.src} alt="vector1" />
            <img className={styleVecter} src={vector1.src} alt="vector1" />
        </div>
    )
}
export default Spring 
