import type { Config } from 'tailwindcss';

const config: Config = {
  content: [
    './src/pages/**/*.{js,ts,jsx,tsx,mdx}',
    './src/components/**/*.{js,ts,jsx,tsx,mdx}',
    './src/app/**/*.{js,ts,jsx,tsx,mdx}',
  ],
  theme: {
    extend: {
      colors: {
        primaryBlue: '#091333',
        KTBlue: '#0082EF',
        textDarkBlue: '#032953',
        'aster-grey-900': '#1A1D23',
        'aster-grey-800': '#2B303B',
        'aster-grey-700': '#414958',
        'aster-grey-600': '#576175',
        'aster-grey-400': '#8A94A8',
        'aster-grey-300': '#CED2DB',
        'aster-grey-200': '#E5E7EC',
        'aster-grey-100': '#F6F7F9',
        'primary-blue-400': '#0183E8',
        'primary-blue-300': '#1FBEFF',
        'primary-blue-200': '#83E9FF',
        'primary-blue-100': '#D6F5FF',
        'primary-violet-400': '#7D25ED',
        'primary-violet-300': '#8E4CFF',
        'primary-violet-200': '#C4ADFF',
        'primary-violet-100': '#EDE7FF',
        'warning-300': '#C49B09',
        'positive-300': '#13CB68',
        'aster-white': '#FFFFFF',
        'gradient-orange': '#ec774c',
        'aster-bg-grey-900': '#1A1D2380',
        'aster-bg-grey-800': '#2B303B80',
      },
      backgroundImage: {
        'gradient-radial': 'radial-gradient(var(--tw-gradient-stops))',
        'gradient-conic':
          'conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))',
      },
      padding: {
        '32': '32px',
        '888': 'var(--spacing-none, 0px)',
      },
      borderWidth: {
        DEFAULT: '1px',
        '1.5': '1.5',
      },
      fontFamily: {
        poppins: ['Poppins', 'sans-serif'],
      },
    },
  },
  plugins: [],
};
export default config;
